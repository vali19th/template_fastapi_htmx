.SHELLFLAGS = -ec  # exit immediately if a command fails

VENV_DIR = $(abspath .venv)
PY = $(VENV_DIR)/bin/python3
PIP = $(PY) -m pip

demo:
	$(MAKE) install
	$(MAKE) reset
	$(MAKE) test || true  # ignore test failures
	$(MAKE) dev

install:
	rm -rf $(VENV_DIR)
	python3 -m venv $(VENV_DIR)
	$(PIP) install --upgrade pip
	$(PIP) install -e .[dev]
	$(PY) -m pre_commit install
	rm -rf build dist **/*.egg-info

dev:
	fastapi dev src/wp_template_fastapi_htmx/main.py

prod:
	fastapi run src/wp_template_fastapi_htmx/main.py

reset:
	rm -rf data/current
	cp -r data/example data/current

test:
	$(PY) -m pytest --doctest-modules --cov=src --cov-report term-missing --failed-first

testp:  # run in parallel
	$(PY) -m pytest --doctest-modules --cov=src --cov-report term-missing --failed-first -n auto

update:
	$(PIP) install --upgrade -e .[dev]
	rm -rf build dist **/*.egg-info

docker-build:
	docker buildx build -t wp_template_fastapi_htmx .

docker-run:
	docker run --env-file .env -p 8000:8000 wp_template_fastapi_htmx

.PHONY: demo install reset run test testp update
