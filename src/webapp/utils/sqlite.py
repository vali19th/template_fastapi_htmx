import re
import sqlite3
from collections import defaultdict
from operator import itemgetter
from pathlib import Path
from pprint import pformat

from katalytic.data.checks import is_iterable, is_sequence
from katalytic.data import flatten, one
from katalytic.files import delete_file, load

from utils.data import subdict


class DB_SQLite:
    def __init__(self, path, check=True):
        self._db = sqlite3.connect(path)

        on_disk = path != ":memory:"
        self._set_pragmas(on_disk=on_disk)

        if check:
            self.check()

    def close(self):
        self.check()
        pragmas = """
            PRAGMA analysis_limit=1000;
            PRAGMA optimize;
        """
        self.exec_script(pragmas)

        self._db.close()
        self._db = None

    def exec(self, statement, data=None):
        cursor = self._db.cursor()
        cursor.execute(statement, data or {})
        self._db.commit()
        cursor.close()
        return cursor.lastrowid

    def exec_many(self, statement, data=None):
        cursor = self._db.cursor()
        cursor.executemany(statement, data or {})
        self._db.commit()
        cursor.close()

    def exec_script(self, script):
        if script.endswith(".sql"):
            script = load(script)

        cursor = self._db.cursor()
        cursor.executescript(script)
        self._db.commit()
        cursor.close()

    def insert(self, data, table, *, iter_callback=lambda x: x):
        DB_SQLite.validate_identifiers(table)
        assert data, "Must provide data to insert"

        if is_sequence(data):
            return [self.insert(row, table) for row in iter_callback(data)]
        elif isinstance(data, dict):
            DB_SQLite.validate_identifiers(data.keys())

            columns = ",".join(data.keys())
            values = ",".join(f":{col}" for col in data.keys())
            stmt = f"INSERT INTO {table}({columns}) VALUES ({values})"
            return self.exec(stmt, data)
        else:
            raise TypeError(f"Expected dict or list of dicts, got {type(data)}")

    def upsert(self, data, table, on_conflict="replace", *, iter_callback=lambda x: x):
        """
        Args:
            data: dict or list of dicts
                The row(s) to insert or update
            table: str
                The table to insert or update the row(s) into
            on_conflict: str, default "replace"
                What to do when there's a conflict
                - "error": raise an error
                - "replace": replace the row
                - "skip_value": skip the value
                - "skip_row": skip the row
            iter_callback: function, default lambda x: x
                A function that takes an iterable and returns an iterable.
                You can use it to add logging or a progress bar (tqdm)
                e.g. upsert(data, table, iter_callback=tqdm)

        TODO:
        - on_conflict += "keep_newest:<key>"
        - refactor into a diff function and an apply function -> easier to test
        """
        if not data:
            return

        DB_SQLite.validate_identifiers(table)

        expected = ("error", "replace", "skip_value", "skip_row")
        assert on_conflict in expected, "".join(
            [
                "Expected on_conflict to be one of: ",
                ", ".join(repr(e) for e in expected),
                f". Got: {on_conflict!r}",
            ]
        )

        pk = self.primary_key(table)
        assert pk is not None, f"There's no PK for {table!r}"
        pk_parameters = DB_SQLite.prepare_PK_parameters(pk, data)
        pk_placeholders = DB_SQLite.prepare_PK_condition_placeholders(pk, data)

        if is_sequence(pk):
            old_rows = self.query(
                f"SELECT * FROM {table} WHERE {pk_placeholders}", pk_parameters
            )
            extract_pk = itemgetter(*pk)
            old_rows = {extract_pk(row): row for row in old_rows}
            new_rows = {extract_pk(row): row for row in data}
        else:
            old_rows = self.query(
                f"SELECT * FROM {table} WHERE {pk} in ({pk_placeholders})",
                pk_parameters,
            )
            old_rows = {row[pk]: row for row in old_rows}
            new_rows = {row[pk]: row for row in data}

        pk_condition = DB_SQLite.prepare_PK_condition_with_key(
            pk, one(new_rows.values())
        )
        for row_pk, new in iter_callback(new_rows.items()):
            old = old_rows.get(row_pk)
            if old is None:
                self.insert(new, table)
                continue

            diff = {c: v for c, v in new.items() if old.get(c) != v}
            for c, v in list(diff.items()):
                DB_SQLite.validate_identifiers(c)

                if old[c] is None:
                    pass
                elif on_conflict == "error" and v is not None:
                    raise ValueError(f"Conflict on {c!r}: {old[c]!r} -> {v!r}")
                elif on_conflict == "replace":
                    pass
                elif on_conflict == "skip_row":
                    diff = {}
                    break
                elif on_conflict == "skip_value":
                    diff.pop(c)

            if diff:
                columns = ", ".join(f"{c} = :{c}" for c in diff)
                self.exec(f"UPDATE {table} SET {columns} WHERE {pk_condition}", new)

    def query(self, query, data=None, *, as_tuples=False):
        cursor = self._db.cursor()
        cursor.execute(query, data or {})
        rows = cursor.fetchall()
        cursor.close()

        if as_tuples:
            return rows
        else:
            header = [col[0] for col in cursor.description]
            return [dict(zip(header, row)) for row in rows]

    def query_row(self, query, data=None, *, as_tuple=False):
        rows = self.query(query, data, as_tuples=as_tuple)
        assert len(rows) == 1, len(rows)
        return rows[0]

    def query_value(self, query, data=None):
        row = self.query_row(query, data, as_tuple=True)
        assert len(row) == 1, len(row)
        return row[0]

    def query_column(self, query, data=None):
        rows = self.query(query, data, as_tuples=True)
        assert all(len(row) == 1 for row in rows), {len(row) for row in rows}
        return [row[0] for row in rows]

    def query_map(self, query, *, data=None, keys=None, exclude_keys=False):
        rows = self.query(query, data)
        if not rows:
            return {}

        if not keys:
            keys = list(rows[0].keys())[:1]
        if isinstance(keys, str):
            keys = [keys]

        get_keys = itemgetter(*keys)
        if exclude_keys:
            return {get_keys(r): subdict(r, keys, exclude=True) for r in rows}
        else:
            return {get_keys(r): r for r in rows}

    def reorder_rowids_by_column(self, table, column, case_insensitive=True):
        DB_SQLite.validate_identifiers(table)
        DB_SQLite.validate_identifiers(column)
        if case_insensitive:
            column = f"{column} COLLATE NOCASE"

        pk = self.primary_key(table)
        tmp_table = f"""
            WITH tmp AS (
                SELECT {pk}, new_id
                FROM (
                    SELECT {pk}, ROW_NUMBER() OVER (ORDER BY {column}) AS new_id
                    FROM {table}
                )
                WHERE {pk} != new_id
            )
        """

        max_id = self.query_value(f"SELECT MAX({pk}) FROM {table}")
        self.exec(
            f"""
            {tmp_table}
            UPDATE {table}
            SET {pk} = (SELECT new_id + {max_id} FROM tmp WHERE {table}.{pk} = tmp.{pk})
            WHERE {pk} IN (SELECT {pk} FROM tmp);
        """
        )

        self.exec(
            f"""
            {tmp_table}
            UPDATE {table}
            SET {pk} = (SELECT new_id FROM tmp WHERE {table}.{pk} = tmp.{pk})
            WHERE {pk} IN (SELECT {pk} FROM tmp);
        """
        )

    def check(self):
        def all_ok(values):
            if values and is_iterable(values[0]):
                return all(all_ok(v) for v in values), values
            else:
                return all(v == "ok" for v in values), values

        check = self.query("PRAGMA integrity_check;", as_tuples=True)
        assert all_ok(check), check

        check = self.query("PRAGMA foreign_key_check;", as_tuples=True)
        assert all_ok(check), check

    def create_updated_trigger(self, tables=None):
        if not tables:
            q = """
                SELECT name
                FROM sqlite_master
                WHERE type='table' AND sql LIKE '%updated%';
            """
            tables = self.query_column(q)
        elif isinstance(tables, str):
            tables = [tables]

        for table in tables:
            DB_SQLite.validate_identifiers(table)

            exists = self.query_value(
                f"""
                SELECT EXISTS(
                    SELECT name FROM sqlite_master
                    WHERE type='trigger' AND name='{table}_update_timestamp'
                );
            """
            )
            if exists:
                continue

            stmt = f"""
                CREATE TRIGGER {table}_update_timestamp
                AFTER UPDATE ON {table}
                BEGIN
                    UPDATE {table} SET updated=CURRENT_TIMESTAMP WHERE rowid=NEW.rowid;
                END;
            """
            self.exec(stmt)

    def diff_table(self, other, table, *, id_keys=None, select_keys="*"):
        DB_SQLite.validate_identifiers(table)

        if not id_keys:
            id_keys = self.primary_key(table)
            assert id_keys, f"No primary key found for {table!r}"

        if select_keys != "*":
            DB_SQLite.validate_identifiers(select_keys)
            select_keys = ", ".join(select_keys)

        rows_1 = self.query_map(f"select {select_keys} from {table}", keys=id_keys)
        rows_2 = other.query_map(f"select {select_keys} from {table}", keys=id_keys)

        diff = {}
        for key in set(rows_1) | set(rows_2):
            row_1 = rows_1.get(key)
            row_2 = rows_2.get(key)
            if row_1 != row_2:
                diff[key] = (row_1, row_2)

        return diff

    def diff_schema(self, other):
        diff_1, diff_2 = [], []
        keys = {
            "name": "column",
            "type": "type",
            "notnull": "not_null",
            "pk": "pk",
            "dflt_value": "default_value",
        }

        get_table_names = "SELECT name FROM sqlite_master WHERE type='table'"
        tables = set(self.query_column(get_table_names))
        tables |= set(other.query_column(get_table_names))

        for t in tables:
            cols_1 = self.query(f"PRAGMA table_info('{t}')")
            cols_2 = other.query(f"PRAGMA table_info('{t}')")

            cols_1 = [subdict(row, keys) for row in cols_1]
            cols_2 = [subdict(row, keys) for row in cols_2]

            diff_1 += [
                {"db": "self", "table": t, **c} for c in cols_1 if c not in cols_2
            ]
            diff_2 += [
                {"db": "other", "table": t, **c} for c in cols_2 if c not in cols_1
            ]

        return sorted(diff_1 + diff_2, key=itemgetter("table", "column", "db"))

    def primary_key(self, table):
        DB_SQLite.validate_identifiers(table)
        columns = self.query(f"PRAGMA table_info('{table}')")
        columns = sorted((c["pk"], c["name"]) for c in columns if c["pk"])

        n = len(columns)
        if n == 0:
            return None
        elif n == 1:
            return columns[0][1]
        else:
            return tuple(c[1] for c in columns)

    def _set_pragmas(self, *, on_disk=True):
        pragmas = """
            PRAGMA extended_result_codes = 1;
            PRAGMA cell_size_check = ON;
            PRAGMA encoding = 'UTF-8';
            PRAGMA foreign_keys = ON;
            PRAGMA ignore_check_constraints = OFF;
            PRAGMA journal_mode = WAL;
            PRAGMA writable_schema = OFF;
            PRAGMA threads = 4;
            PRAGMA cache_size = -102400; -- 102400 KiB = 100 MB
        """
        self.exec_script(pragmas)

        if on_disk:
            pragmas = """
                PRAGMA secure_delete = FULL;
                PRAGMA synchronous = FULL;
                PRAGMA temp_store = MEMORY;
                PRAGMA mmap_size = 268435456; -- 268435456 Bytes = 256 MB
            """
            self.exec_script(pragmas)

    @staticmethod
    def prepare_PK_condition_placeholders(pk, data):
        if isinstance(data, dict):
            data = [data]

        if isinstance(pk, str):
            return "?" + ", ?" * (len(data) - 1)
        else:
            pk_combo = "(" + " AND ".join(f"{col} = ?" for col in pk) + ")"
            return f"{pk_combo}" + f" OR {pk_combo}" * (len(data) - 1)

    @staticmethod
    def prepare_PK_condition_with_key(pk, data):
        if isinstance(data, dict):
            data = [data]

        if isinstance(pk, str):
            return f"{pk} = :{pk}"
        else:
            pk_combo = " AND ".join(f"{col} = :{col}" for col in pk)
            return f"{pk_combo}" + f" OR {pk_combo}" * (len(data) - 1)

    @staticmethod
    def prepare_PK_parameters(PK, data):
        if isinstance(data, dict):
            data = [data]

        if isinstance(PK, str):
            return [row[PK] for row in data]
        else:
            extract_PK = itemgetter(*PK)
            return flatten(extract_PK(row) for row in data)

    @staticmethod
    def validate_identifiers(identifiers):
        if isinstance(identifiers, str):
            assert str.isidentifier(
                identifiers
            ), f"{identifiers!r} is not a valid identifier"
        else:
            errors = []
            for i in identifiers:
                if not str.isidentifier(i):
                    errors.append(f"{i!r} is not a valid identifier")

            assert not errors, errors

    @staticmethod
    def merge_databases(
        db_1,
        db_2,
        db_3,
        tables=None,
        *,
        iter_callback=(lambda x: x),
        formatting_fn=pformat,
    ):
        """
        Merge the rows from db_1 and db_2 into db_3.
        If there are conflicts, print them and exit.

        Args:
            db_1: DB_SQLite
                The first database to merge.
            db_2: DB_SQLite
                The second database to merge.
            db_3: DB_SQLite
                The database to merge into.
                It should have the same schema and the tables should be empty
            tables: list of str, default None
                The tables to merge. If None, all tables are merged.
            iter_callback: function, default None
                A function that takes an iterable and returns an iterable.
                You can use it to add logging or a progress bar (tqdm)
                e.g. merge_databases(db_1, db_2, db_3, iter_callback=tqdm)
            formatting_fn: function, default pprint.pformat
                A function that takes a list of dicts and returns a string.
                For example, you can format it as a table:
                    from functools import partial
                    format = partial(tabulate.tabulate, tablefmt="pipe", headers="keys", missingval="?")
        """
        DB_SQLite.ensure_schemas_are_compatible(db_1, db_3, formatting_fn)
        DB_SQLite.ensure_schemas_are_compatible(db_2, db_3, formatting_fn)

        final = defaultdict(list)
        impossible_to_update = defaultdict(list)
        if not tables:
            tables = db_1.query_column(
                "SELECT name FROM sqlite_master WHERE type='table'"
            )

        for table in iter_callback(tables):
            pk = db_1.primary_key(table)
            diff = db_1.diff_table(db_2, table)
            final[table] = DB_SQLite._find_rows_common_to_both_DBs(
                db_1, diff, pk, table
            )

            for id, (row_1, row_2) in iter_callback(diff.items()):
                if row_1 is None:
                    final[table].append(row_2)
                elif row_2 is None:
                    final[table].append(row_1)
                elif row_1["updated"] < row_2["updated"]:
                    final[table].append(row_2)
                else:
                    impossible_to_update[table].append(row_1)
                    impossible_to_update[table].append(row_2)

        if impossible_to_update:
            DB_SQLite._print_conflicts_and_exit(impossible_to_update, formatting_fn)

        for table, rows in final.items():
            if rows:
                db_3.upsert(rows, table)

    @staticmethod
    def ensure_schemas_match(db_1, db_2, formatting_fn=pformat):
        """
        # If you want to print the diff as a table:
        from functools import partial
        format = partial(tabulate.tabulate, tablefmt="pipe", headers="keys", missingval="?")
        ensure_schemas_match(db_1, db_2, format)
        """
        diff = db_1.diff_schema(db_2)
        if diff:
            raise RuntimeError(f"Column mismatch:\n{formatting_fn(diff)}")

    @staticmethod
    def ensure_schemas_are_compatible(db_1, db_2, formatting_fn=pformat):
        """
        The function assumes you want to transfer data from db_1 to db_2. The order matters

        # If you want to print the diff as a table:
        from functools import partial
        format = partial(tabulate.tabulate, tablefmt="pipe", headers="keys", missingval="?")
        ensure_schemas_are_compatible(db_1, db_2, format)
        """
        diff = db_1.diff_schema(db_2)
        diff = sorted(diff, key=itemgetter("db"), reverse=True)

        get = itemgetter("table", "column")
        grouped = defaultdict(list)
        for row in diff:
            grouped[get(row)].append(row)

        new_diffs = []
        for g, rows in grouped.items():
            if len(rows) == 1:
                row = rows[0]
                if row["db"] == "other" and row["not_null"] == 0 and row["pk"] == 0:
                    # this is a new column in db_2 and it does not require a NOT NULL value
                    # No data is lost and no constraint is violated
                    diff.remove(row)

                continue

            keys = set(rows[0].keys()) | set(rows[1].keys())
            keys -= {"db", "table", "column"}
            row_diff = {
                k: (rows[0][k], rows[1][k]) for k in keys if rows[0][k] != rows[1][k]
            }

            not_null = row_diff.get("not_null")
            if not_null and not_null[1] == 0:
                # The *NOT NULL* constraint is removed in db_2
                row_diff.pop("not_null")

            pk = row_diff.get("pk")
            if pk and pk[1] == 0:
                # The *PRIMARY KEY* constraint is removed in db_2
                # Primary keys imply a *UNIQUE* constraint
                row_diff.pop("pk")

            type_ = row_diff.get("type")
            if type_ and DB_SQLite._is_conversion_safe(*type_):
                row_diff.pop("type")

            if row_diff:
                row_1 = {"db": "self", "table": g[0], "column": g[1]}
                row_1.update((k, v[0]) for k, v in row_diff.items())

                row_2 = {"db": "other", "table": g[0], "column": g[1]}
                row_2.update((k, v[1]) for k, v in row_diff.items())

                new_diffs.append(row_1)
                new_diffs.append(row_2)

        if new_diffs:
            raise RuntimeError(f"Column mismatch:\n{formatting_fn(new_diffs)}")

    @staticmethod
    def setup(db_path, script, *, overwrite=False):
        """
        Args:
            db_path: the path to the database
            script: either an .sql path or a str representing the DSL statements to execute
            overwrite: bool, default False
                If True, delete the file if it exists
                If False and the file exists, raise a FileExistsError

        Returns: a DB_SQLite instance with the schema defined in the .sql files
        """
        if overwrite:
            delete_file(db_path)
        elif Path(db_path).exists():
            raise FileExistsError(f"{db_path!r} already exists")

        db = DB_SQLite(db_path)
        db.exec_script(script)
        db.create_updated_trigger()
        return db

    @staticmethod
    def _print_conflicts_and_exit(impossible_to_update, formatting_fn=pformat):
        """
        # If you want to print the diff as a table:
        from functools import partial
        formatting_fn = partial(tabulate.tabulate, tablefmt="pipe", headers="keys", missingval="?")
        print_conflicts_and_exit(impossible_to_update, formatting_fn)
        """
        # flatten by moving the table name into the rows and
        # improve readability by removing the columns that have all values set to None
        conflicts = [
            {"table": table, **{k: v for k, v in row.items() if v is not None}}
            for table, rows in impossible_to_update.items()
            for row in rows
        ]
        raise RuntimeError(f"Impossible to update:\n{formatting_fn(conflicts)}")

    @staticmethod
    def _find_rows_common_to_both_DBs(db, diff, pk, table):
        """If you want to make it public, you should generalize it
        to not depend on the *diff* arg"""
        if not diff:
            return db.query(f"SELECT * FROM {table}")

        pks_with_diffs = [{pk: k} for k in diff.keys()]
        pk_parameters = DB_SQLite.prepare_PK_parameters(pk, pks_with_diffs)
        pk_placeholders = DB_SQLite.prepare_PK_condition_placeholders(
            pk, pks_with_diffs
        )

        if is_sequence(pk):
            pk_condition = f"NOT ({pk_placeholders})"
        else:
            pk_condition = f"{pk} NOT IN ({pk_placeholders})"

        return db.query(f"SELECT * FROM {table} WHERE {pk_condition}", pk_parameters)

    @staticmethod
    def _is_conversion_safe(src, dest):
        """
        Compare the types src and dest and return True if there's a guaranteed
        implicit conversion without any data or precision loss from src to dest.

        If the conversion needs an explicit cast, return False.
        """
        # Do not change the order of the first 4 conditions
        # src == dest, dest == "BLOB", src == "BLOB" and dest != "BLOB". dest == "TEXT"

        # fmt: off
        if src == dest:
            return True
        elif dest == "BLOB":
            return True
        elif src == "BLOB" and dest != "BLOB":
            return False
        elif dest == "TEXT":
            return True
        elif src in ("CHAR", "VARCHAR") and dest in ("CHAR", "VARCHAR"):
            return True
        elif src in ("DATE", "TIME") and dest == "DATETIME":
            return True
        elif src == "TINYINT" and dest in ("SMALLINT", "INT", "INTEGER", "BIGINT", "FLOAT", "REAL", "DOUBLE"):
            return True
        elif src == "SMALLINT" and dest in ("INT", "INTEGER", "BIGINT", "FLOAT", "REAL", "DOUBLE"):
            return True
        elif src in ("INT", "INTEGER") and dest in ("INT", "INTEGER", "BIGINT"):
            return True
        elif src in ("FLOAT", "DOUBLE", "REAL") and dest in ("FLOAT", "DOUBLE", "REAL"):
            # All floating point types are treated identically by sqlite.
            # INT to FLOAT combos are not included because floats lose precision
            # for integers larger than 63 bits.
            return True
        else:
            return False
        # fmt: on


def query(data, query):
    db = DB_SQLite(":memory:")
    one_line_query = query.replace("\n", " ")

    table = re.search(r"FROM (\w+)", one_line_query)[1]
    DB_SQLite.validate_identifiers(table)

    keys = {k for row in data for k in row.keys()}
    DB_SQLite.validate_identifiers(keys)
    keys = ", ".join(keys)

    db.exec(f"CREATE TABLE {table} ({keys})")
    db.insert(data, table)
    return db.query(query)
