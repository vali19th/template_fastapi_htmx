# Setup
```bash
git clone git@gitlab.com/vali19th/template_fastapi_htmx.git
cd template_fastapi_htmx

make install
make reset
make dev  # to start the server in dev mode
```

# Run
```bash
./run.sh
```
